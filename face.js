export const happyFace = {
    mood: "happy",
    img: "https://res.cloudinary.com/davbdgyux/image/upload/v1573262149/chatbot/happy_kgeiic.png"
};
const weirdFace = {
    mood: "weird",
    img: "https://res.cloudinary.com/davbdgyux/image/upload/v1573262149/chatbot/weird_t46qzx.png"
};
const joyFace = {
    mood: "joy",
    img: "https://res.cloudinary.com/davbdgyux/image/upload/v1573262149/chatbot/joy_nukn17.png"
};

const faces = [happyFace, weirdFace, joyFace];

export function getNextFace(currentFace) {
    let faceIndex = getFaceIndex(currentFace);
    if (faceIndex === undefined) { throw 'Not a valid face'; }
    if (faceIndex + 1 === faces.length) { return faces[0]; }
    return faces[faceIndex + 1];
}

function getFaceIndex(currentFace) {
    for (const [index, face] of faces.entries()) {
        if (currentFace.mood === face.mood) {
            return index;
        }
    }
};