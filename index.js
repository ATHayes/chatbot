import {getNextFace} from '/face'
import {happyFace} from "/face";
import {line1, getNextLine} from "./src/dialog/dialog";
let faceState = happyFace;
let lineState = line1;
let buttonImg = "https://res.cloudinary.com/davbdgyux/image/upload/v1574823927/Screenshot_2019-11-26_at_22.04.58_crxcay.png";
let hoverButtonImg = "https://res.cloudinary.com/davbdgyux/image/upload/v1574824286/Screenshot_2019-11-26_at_22.11.17_iuvcrv.png";


function cycleFace() {
    faceState = getNextFace(faceState);
    changeFaceImage(faceState);
}

function changeFaceImage(face) {
    document.getElementById("faceImage").src = face.img;
}

function changeLine() {
    lineState = getNextLine(lineState);
    uiChangeLine(lineState);
}

function uiChangeLine() {
    // window.requestAnimationFrame(uiChangeLine)
    //
    // for (let i = 1; i < 2; i++) {
    //         window.requestAnimationFrame(uiChangeLine);
    //         let partialLine = lineState.line.substring(0, 1);
    //         document.getElementById("wordsfrombot").innerHTML = lineState;
    // }

}

document.onreadystatechange = function () {
    if (document.readyState === "interactive") {
        changeFaceImage(faceState);
        uiChangeLine(lineState);
        document.getElementById("faceImage").addEventListener("click", cycleFace);
        // document.getElementById("button2").addEventListener("click", cycleFace);
        document.getElementById("button2").addEventListener("click", changeLine);
    }
};


