export const line1 = {index: 0, line: `This is the first line of dialog.`};
export const line2 = {index: 1, line: `This is the second line of dialog.`};
export const line3 = {index: 2, line: `This is the third line of dialog.`};

const linesOfDialog = [line1, line2, line3];

export function getNextLine(line) {
    if (line.index + 1 === linesOfDialog.length) { return linesOfDialog[0]; }
    return linesOfDialog[line.index + 1];
}